
from datetime import datetime
from dateutil import tz #timezone
from script.conection import 
import time, json

 # função responsável por fazer a conexao com api iqoption
 # email e senha do iqoption
API = IQ_Option('samuel_sankys@hotmail.com', 'samuelsk8')
API.set_max_reconnect(5) # quantidade max de vezes p/ reconectar, -1 = infinito, 0 = 1 vez, 5 = 5 tentativas

# processo de conexão na iqOption
while True:

    if API.check_connect() == False: # Se o estado de conexao for falso
        print('Erro ao se conectar')
        API.reconnect()
    else:
        print('Conectado com sucesso')
        break
    
    time.sleep(1)



# Seleciona o tipo de conta de que quer usar, se conta real ou pratica
def select_tipe_count(tipe):
    API.change_balance(tipe) # MUDAR DE CONTA REAL E DE PRATICA (PRACTICE E REAL)

#função conversora de timestamp para um formato comum ao 
def timestamp_converter(x):
    # Convertendo para formato de data tradicional no formato mostrado
    hora = datetime.strptime(datetime.utcfromtimestamp(x).strftime('%Y-%m,-%d %H:%M:%S'), '%Y-%m,-%d %H:%M:%S')
    # Colocando para o horário do brasil.
    hora = hora.replace(tzinfo = tz.gettz('GMT'))
    
    # Conversão completa de timestamp para nosso horario local - neste caso horario de SP que deveria ser o horário que a iqoption está config
    return str(hora.astimezone(tz.gettz('America/São Paulo'))) [:-6]

def perfil():
    perfil = json.loads(json.dumps(API.get_profile()))

    return perfil['result']
    '''
        name
        first_name
        last_name
        email
        city
        nickname
        currency   # a moeda que está utilizando ex. USD - dolar
        currency_char   # $ ou R$ dependendo da moeda
        address
        created     #quando a conta foi criada
        postal_index 
        gender      # Gênero - masculino ou feminino
        birthdate   # data de nascimento
        balance     #banca
    '''

def payout(par1, tipo, timeframe = 1):
    if tipo == 'turbo':
        a = API.get_all_profit() # retorna todas as payout disponíveis do turbo e binary
        return int(100*a[par1]['turbo']) #retorna somente a do turbo em porcentagem
    elif tipo == 'digital':
        API.subscribe_strike_list(par1, timeframe)
        while True:
            d = API.get_digital_current_profit(par1, timeframe)
            if d != False:
                d = int(d)
                break
            time.sleep(1)
        API.unsubscribe_strike_list(par1, timeframe)
        return d





par = 'EURUSD' # moeda a ser utilizada
# parâmetros 1º moeda, 2º seguntos a duração  da vela, 3º quantidade de velas no intervalo de tempo anterior
# 4º retorna o valor do tempo atual que está em formato timestamp
vela = API.get_candles(par, 60, 10, time.time())

# imprimindo várias velas (de acordo com o 3º parametro)- valor max 1000 velas
for velas in vela:
    print('Hora início: '+str(timestamp_converter(velas['from'])) + ' Abertura: ' + str(velas['open']))

##############  IMPRESSÕES  ################

#print(perfil()) # Faz ver todos os dados que contem o perfil
'''
x = perfil()
print(x['name'])
print(x['nickname'])
print(x['currency'])
print(API.get_balance()) # imprime o valor que tem disponível na conta ('PRACTICE' OR 'REAL')
'''

# imprimindo mais de 1000 velas ------------------------
'''
total = [] # array  com todos os valores das velas
tempo = time.time() # tempo atual.

for i in range(2): # range conta de 1 a 2
    X = API.get_candles(par, 60, 1000, tempo)
    total = X + total
    tempo = int(X[0]['from'])-1

for velas in total:
    print(timestamp_converter(velas['from']))
'''

# Descobrindo quais paridades estão abertas
par1 = API.get_all_open_time()

#print(par1) # mostra todas as paridades

for paridade in par1['turbo']:
    # retorna ,as que estão abertas, da lista de turbo o campo open 
    if par1['turbo'][paridade]['open'] == True:
        print('[TURBO]:'+ paridade + ' | Payout: '+str(payout(paridade, 'turbo')))
print('\n')

for paridade in par1['digital']:
    # retorna ,as que estão abertas, da lista de turbo o campo open 
    if par1['digital'][paridade]['open'] == True:
        print('[DIGITAL]: '+paridade+' | Payout: '+str(payout(paridade, 'digital')))


####################################################################################
# Retorna o histórico, para pegar o histórico do digital, deve ser colocado 'digital-option' e para pegar binario, 
#	deve ser colocado 'turbo-option'
status,historico = API.get_position_history_v2('turbo-option', 7, 0, 0, 0)

'''

:::::::::::::::: [ MODO DIGITAL ] ::::::::::::::::
FINAL OPERACAO : historico['positions']['close_time']
INICIO OPERACAO: historico['positions']['open_time']
LUCRO          : historico['positions']['close_profit']
ENTRADA        : historico['positions']['invest']
PARIDADE       : historico['positions']['raw_event']['instrument_underlying']
DIRECAO        : historico['positions']['raw_event']['instrument_dir']
VALOR          : historico['positions']['raw_event']['buy_amount']

:::::::::::::::: [ MODO BINARIO ] ::::::::::::::::
MODO TURBO tem as chaves do dict diferentes para a direção da operação(put ou call) 
	e para exibir a paridade, deve ser utilizado:
DIRECAO : historico['positions']['raw_event']['direction']
PARIDADE: historico['positions']['raw_event']['active']
'''

for x in historico['positions']:
	print('PAR: '+str(x['raw_event']['active'])+' /  DIRECAO: '+str(x['raw_event']['direction'])+' / VALOR: '+str(x['invest']))
	print('LUCRO: '+str(x['close_profit'] if x['close_profit'] == 0 else round(x['close_profit']-x['invest'], 2) ) + ' | INICIO OP: '+str(timestamp_converter(x['open_time'] / 1000))+' / FIM OP: '+str(timestamp_converter(x['close_time'] / 1000)))
	print('\n')


    ##############################################################################33
   
par = 'EURUSD'
entrada = 2
direcao = 'call'
timeframe = 1

# Entradas na digital
id = API.buy_digital_spot(par, entrada, direcao, timeframe)

if isinstance(id, int):
	while True:
		status,lucro = API.check_win_digital_v2(id)
		
		if status:
			if lucro > 0:
				print('RESULTADO: WIN / LUCRO: '+str(round(lucro, 2)))
			else:
				print('RESULTADO: LOSS / LUCRO: '+str(entrada))
			break


# Entradas na binaria
status,id = API.buy(entrada, par, direcao, timeframe)

if status:
	resultado,lucro = API.check_win_v3(id)
	
	print('RESULTADO: '+resultado+' / LUCRO: '+str(round(lucro, 2)))