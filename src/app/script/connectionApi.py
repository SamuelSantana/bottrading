# Standard library imports
import time
import sys
# Third party imports
from iqoptionapi.stable_api import IQ_Option

# Local application imports




def loginApi(login, password):
    error_password="""{"code":"invalid_credentials","message":"You entered the wrong credentials. Please check that the login/password is correct."}"""
    API = IQ_Option(login, password)
    #API.set_max_reconnect(5) # quantidade max de vezes p/ reconectar, -1 = infinito, 0 = 1 vez, 5 = 5 tentativas
    check, reason = API.connect()
    # processo de conexão na iqOption
    if check:
        #start bot
        while True:
            if API.check_connect() == False: # Se o estado de conexao for falso
                print('Tentando Reconectar')
                
                check, reason = API.connect()
                if check:
                    print('Reconectado com sucesso')
                    
                    return API
                else: 
                    if reason==error_password:
                        print('Senha incorreta')
                        
                    else:
                        print('Sem internet')
                        
            else: 
                print('Conectado com sucesso')
              
                return API
    else:
        if reason=="[Errno -2] Name or service not known":
            print('Sem internet')
            
        elif reason==error_password:
            print('Senha incorreta')
            
        
    return API
