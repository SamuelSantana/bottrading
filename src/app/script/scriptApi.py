from iqoptionapi.stable_api import IQ_Option
from datetime import datetime, timedelta
from dateutil import tz #timezone
import time, json

def timestamp_converter(x):
    # Convertendo para formato de data tradicional no formato mostrado entre parentese
    hora = datetime.strptime(datetime.utcfromtimestamp(x).strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
    # Colocando para o horário do brasil.
    hora = hora.replace(tzinfo = tz.gettz('GMT'))
   
    
    # Conversão completa de temistamp para nosso horario local - neste caso horario de SP que deveria ser o horário que a iqoption está config
    return str(hora.astimezone(tz.gettz('America/São Paulo'))) [:-6]


def perfil(API):
    perfil = json.loads(json.dumps(API.get_profile()))

    return perfil['result']
    '''
        name
        first_name
        last_name
        email
        city
        nickname
        currency   # a moeda que está utilizando ex. USD - dolar
        currency_char   # $ ou R$ dependendo da moeda
        address
        created     #quando a conta foi criada
        postal_index 
        gender      # Gênero - masculino ou feminino
        birthdate   # data de nascimento
        balance     #banca
    '''

def payout(API, par1, tipo, timeframe = 1):
    if tipo == 'turbo':
        a = API.get_all_profit() # retorna todas as payout disponíveis do turbo e binary
        return int(100*a[par1]['turbo']) #retorna somente a do turbo em porcentagem
    elif tipo == 'digital':
        API.subscribe_strike_list(par1, timeframe)
        while True:
            d = API.get_digital_current_profit(par1, timeframe)
            if d != False:
                d = int(d)
                break
            time.sleep(1)
        API.unsubscribe_strike_list(par1, timeframe)
        return d


def captura_candles(API,paridade, duracao_candle, quant_candle):
#par = 'EURUSD' # moeda a ser utilizada
# parâmetros 1º moeda, 2º seguntos a duração  da vela, 3º quantidade de velas no intervalo de tempo anterior
# 4º retorna o valor do tempo atual que está em formato timestamp
    quant_candle = int(quant_candle)
    paridade = str(paridade)
    if quant_candle > 1000:
       
        repeat = int(quant_candle/1000)
        resto = int(quant_candle%1000)
       
        total = [] # array  com todos os valores das velas
        tempo = time.time() # tempo atual.

        for i in range(repeat): 
            X = API.get_candles(paridade, duracao_candle, 1000, tempo)
            total = X + total
            tempo = int(X[0]['from'])-1

        if resto != 0:
            X = API.get_candles(paridade, duracao_candle, resto+1, tempo)
            total = X + total
        return total
    else:    
        ret = API.get_candles(paridade, duracao_candle, quant_candle+1, time.time())
            
        return ret
