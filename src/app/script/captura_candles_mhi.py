
import time
from datetime import datetime, timedelta

#importes locais
from iqoptionapi.stable_api import IQ_Option
from connectionApi import loginApi
from scriptApi import captura_candles, timestamp_converter
from catalogacao_mhi import catalogacao_mhi


def dadosMetodo_Gedex(API, periodo_velas, periodo_captura, paridade):
    quantidade_de_velas = int(periodo_captura/periodo_velas)
    candles = captura_candles(API, paridade, periodo_velas, quantidade_de_velas)
    # print(candles)

    # Conversao de tempo
    #time_candles = timestamp_converter(candles[0]['from'])
    #print(time_candles)

    #Catalogação das velas GX
    dados = []

    for i in range(quantidade_de_velas):
        dados.append(
            {'id':candles[i]['id'], 
        'from':timestamp_converter(candles[i]['from']),
        'open':candles[i]['open'],
        'close':candles[i]['close'],
        'cor': int('-1')} #velas vermelhas por padrão
        )

        # setando as cores das velas
        if dados[i]['open'] < dados[i]['close']: # verde
            dados[i]['cor'] = 1 
        elif dados[i]['open'] == dados[i]['close']: #doji
            dados[i]['cor'] = 0     

    # função que faz toda a catalogação
    catalog = catalogacao_mhi(dados, quantidade_de_velas)
    return (catalog) 
