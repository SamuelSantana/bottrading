from iqoptionapi.stable_api import IQ_Option
import logging
import time, json
from datetime import datetime, date, timedelta
from dateutil import tz


def timestamp_converter(x, retorno=1):
    hora = datetime.strptime(
        datetime.utcfromtimestamp(x).strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S"
    )
    hora = hora.replace(tzinfo=tz.gettz("GMT"))

    return (
        str(hora.astimezone(tz.gettz("America/Sao Paulo")))[:-6]
        if retorno == 1
        else hora.astimezone(tz.gettz("America/Sao Paulo"))
    )


# logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(message)s')
I_want_money = IQ_Option("samuel_sankys@hotmail.com", "samuelsk8")
I_want_money.connect()  # connect to iqoption
while_run_time = 10

# For digital option

name = "live-deal-digital-option"  # "live-deal-binary-option-placed"/"live-deal-digital-option"
active = "EURUSD"
_type = "PT1M"  # "PT1M"/"PT5M"/"PT15M"
buffersize = 100  #
print("_____________subscribe_live_deal_______________")
I_want_money.subscribe_live_deal(name, active, _type, buffersize)


start_t = time.time()
# while True:
#     # data size is below buffersize
#     # data[0] is the last data
#     data = I_want_money.get_live_deal(name, active, _type)
#     print("__For_digital_option__ data size:" + str(len(data)))
#     print(data)
#     print("\n\n")
#     time.sleep(0.1)
#     # if time.time() - start_t > while_run_time:
#     #     break
# print("_____________unscribe_live_deal_______________")
# I_want_money.unscribe_live_deal(name, active, _type)
def banca():
    return I_want_money.get_balance()


def configuracao():

    # id : 45316621 cangaceiro
    # id : 22756290 -> Lyra
    # id : 44510605 -> Meu
    return {
        "seguir_ids": "",
        "stop_win": "1000",
        "stop_loss": "1000",
        "payout": 0,
        "banca_inicial": banca(),
        "filtro_diferenca_sinal": "2",
        "martingale": "n",
        "sorosgale": "s",
        "niveis": "2",
        "filtro_pais": "todos",
        "filtro_top_traders": "0",
        "valor_minimo": 100,
        "paridade": "EURUSD",
        "valor_entrada": "500",
        "timeframe": "1",
    }


config = configuracao()


def payout(par, tipo, timeframe=1):
    if tipo == "turbo":
        a = I_want_money.get_all_profit()
        return int(100 * a[par]["turbo"])

    elif tipo == "digital":

        I_want_money.subscribe_strike_list(par, timeframe)
        while True:
            d = I_want_money.get_digital_current_profit(par, timeframe)
            if d != False:
                d = int(d)
                break
            time.sleep(1)
        I_want_money.unsubscribe_strike_list(par, timeframe)
        return d


def martingale(tipo, valor, payout):
    if tipo == "simples":
        return valor * 2.2
    else:

        lucro_esperado = float(valor) * float(payout)
        perca = valor
        while True:
            if round(float(valor) * float(payout), 2) > round(
                float(abs(perca)) + float(lucro_esperado), 2
            ):
                return round(valor, 2)
                break
            valor += 0.01


def entradas(config, entrada, direcao, timeframe):
    status, id = I_want_money.buy(
        int(entrada), config["paridade"], direcao, int(timeframe)
    )
    if status:
        print("Entrou na compra")
        # STOP WIN/STOP LOSS
        banca_att = banca()
        stop_loss = False
        stop_win = False

        if round((banca_att - float(config["banca_inicial"])), 2) <= (
            abs(float(config["stop_loss"])) * -1.0
        ):
            stop_loss = True

        if round(
            (banca_att - float(config["banca_inicial"]))
            + (float(entrada) * float(config["payout"]))
            + float(entrada),
            2,
        ) >= abs(float(config["stop_win"])):
            stop_win = True
        polling_time = 2
        while True:

            lucro = I_want_money.check_win_v3(id)
            print(I_want_money.check_win_v3(id))
            if lucro:
                if lucro > 0:
                    return "win", round(lucro, 2), stop_win
                else:
                    return "loss", 0, stop_loss
                break

    else:
        return "error", 0, False


# For binary option  61147439 - verificar os dados do usuario


name = "live-deal-binary-option-placed"
active = "EURUSD"  # AUDCAD  NZDUSD  EURGBP
_type = "turbo"  # "turbo"/"binary"
buffersize = 10  #
print("_____________subscribe_live_deal_______________")
# I_want_money.subscribe_live_deal(name, active, _type, buffersize)
old = 0
start_t = time.time()
count = 0
while True:
    I_want_money.subscribe_live_deal(name, active, _type, buffersize)
    while True:
        # data size is below buffersize
        # data[0] is the last data
        data = I_want_money.get_live_deal(name, active, _type)
        # if len(data) > 0:
        #     print(data[0])
        if (
            len(data) > 0
            and old != data[0]["option_id"]
            and data[0]["amount_enrolled"] >= float(config["valor_entrada"])
            # and data[0]["user_id"] == str(45316621)
        ):
            print("__For_binary_option__ data size:" + str(len(data)))
            # print("[ " + str(data[0]["user_id"]) + " ] " + data[0]["name"])
            # print(
            #     "Valor: "
            #     + str(data[0]["amount_enrolled"])
            #     + " "
            #     + str(data[0]["expiration"])
            # )
            print(data[0])
            print("\n\n")

            res = round(
                time.time()
                - datetime.timestamp(
                    timestamp_converter(data[0]["created_at"] / 1000, 2)
                ),
                2,
            )
            ok = True if res <= int(1) else False

            # if len(filtro_top_traders) > 0:
            #     if data[0]["user_id"] not in filtro_top_traders:
            #         ok = False

            if ok:
                # Dados sinal

                print(res, end="")
                print(
                    " [",
                    data[0]["flag"],
                    "]",
                    active,
                    "/",
                    data[0]["amount_enrolled"],
                    "/",
                    data[0]["direction"],
                    "/",
                    data[0]["name"],
                    data[0]["user_id"],
                )

                # 1 entrada
                resultado, lucro, stop = entradas(
                    config,
                    config["valor_entrada"],
                    data[0]["direction"],
                    int(config["timeframe"]),
                )
                print("   -> ", resultado, "/", lucro, "\n\n")

                # if stop:
                #     print("\n\nStop", resultado.upper(), "batido!")
                #     # sys.exit()

                # # Martingale
                # if resultado == "loss" and config["martingale"] == "S":
                #     valor_entrada = martingale(
                #         "auto", float(config["valor_entrada"]), float(config["payout"])
                #     )
                #     for i in range(
                #         int(config["niveis"]) if int(config["niveis"]) > 0 else 1
                #     ):

                #         print("   MARTINGALE NIVEL " + str(i + 1) + "..", end="")
                #         resultado, lucro, stop = entradas(
                #             config,
                #             valor_entrada,
                #             data[0]["direction"],
                #             int(config["timeframe"]),
                #         )
                #         print(" ", resultado, "/", lucro, "\n")
                #         if stop:
                #             print("\n\nStop", resultado.upper(), "batido!")
                #             # sys.exit()

                #         if resultado == "win":
                #             print("\n")
                #             break
                #         else:
                #             valor_entrada = martingale(
                #                 "auto", float(valor_entrada), float(config["payout"])
                #             )
                #     data = 0

                # elif resultado == "loss" and config["sorosgale"] == "S":  # SorosGale

                #     if float(config["valor_entrada"]) > 5:

                #         lucro_total = 0
                #         lucro = 0
                #         perca = float(config["valor_entrada"])
                #         # Nivel
                #         for i in range(
                #             int(config["niveis"]) if int(config["niveis"]) > 0 else 1
                #         ):

                #             # Mao
                #             for i2 in range(2):

                #                 if lucro_total >= perca:
                #                     break

                #                 print(
                #                     "   SOROSGALE NIVEL "
                #                     + str(i + 1)
                #                     + " | MAO "
                #                     + str(i2 + 1)
                #                     + " | ",
                #                     end="",
                #                 )

                #                 # Entrada
                #                 resultado, lucro, stop = entradas(
                #                     config,
                #                     (perca / 2) + lucro,
                #                     data[0]["instrument_dir"],
                #                     int(config["timeframe"]),
                #                 )
                #                 print(resultado, "/", lucro, "\n")
                #                 if stop:
                #                     print("\n\nStop", resultado.upper(), "batido!")
                #                     # sys.exit()

                #                 if resultado == "win":
                #                     lucro_total += lucro
                #                 else:
                #                     lucro_total = 0
                #                     perca += perca / 2
                #                     break

            old = data[0]["option_id"]
            # I_want_money.pop_live_deal(name, active, _type)

            time.sleep(0.1)

        if len(data) >= 10:
            I_want_money.pop_live_deal(name, active, _type)
            I_want_money.pop_live_deal(name, active, _type)
            I_want_money.pop_live_deal(name, active, _type)
            I_want_money.pop_live_deal(name, active, _type)


print("_____________unscribe_live_deal_______________")
I_want_money.unscribe_live_deal(name, active, _type)
