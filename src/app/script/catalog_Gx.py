
# A função catalogação tem como função receber os dados organizados a plataforma iqoption
# como parametro e a quantidade de velas e tem como retorno gx1, gx2, gx3, gain de cada gx,
# gale de cada gx, hit de cada gx 

# o tempo de análise sempre será no tempo de 5 min por quadrante e cada vela com 1 min de duração

# CATALOGAÇÃO ========================================
def catalogacao_gedex(dados, quantidade_de_velas):
    gx1 = []
    gx2 = []
    gx3 = []
    soma_candles_quad_todo = 0
    gain_gx1 = 0
    gain_gx2 = 0
    gain_gx3 = 0
    gale_gx1 = 0
    gale_gx2 = 0
    gale_gx3 = 0
    hit_gx1 = 0
    hit_gx2 = 0
    hit_gx3 = 0

    # variaveis auxiliar para não começar a definir no primeiro quadrante
    count = 0 
    inicio_contagem = 10 
   
    for i in range(quantidade_de_velas):
        #retirar erro de captura de uma vela futura
        if i == (quantidade_de_velas-1):
            time_fim = dados[i]['from']
            break
        # Contador de operações
        count +=1

        quadrante = dados[i]['from']
        
        #define onde começará a catalogação dos dados
        if (quadrante[15] == '5' or quadrante[15] == '0') and count < 6 : # verifica se é 5 min ou 0 min na hora
            inicio_contagem = i
            time_inicio = dados[i]['from']
            

        #define o início de um quadrante
        # zerar contagens de cor de velas fazendo com que os dados sejam armazenados a cada quadrante
        if (quadrante[15] == '5' or quadrante[15] == '0'):
            
            # só fará a contagem quando iniciar o segundo quadrante
            # preenche o objeto de gx's
            if count > 5:
                # contagem das velas em gx2
                soma_candles_3Ultimas = dados[i-1]['cor'] + dados[i-2]['cor'] + dados[i-3]['cor']

                seg_candle = i
                seg_candle +=1
                

                #Preenchendo vetor gx1
                    #Com minoria vermelha
                if (soma_candles_quad_todo > 0):
                    if  dados[i]['cor'] == -1 : # se a primeira vela do quadrante proximo for vermelha
                        gx1.append('1')
                        gain_gx1 += 1
                    elif dados[seg_candle]['cor'] == -1 : # se a segunda vela do quadrante proximo for vermelha
                        gx1.append('+')
                        gale_gx1 +=1
                    else: # se não acontece no proximo quadrante nem na primeira nem na segunda a vela vermelha
                        gx1.append('0')
                        hit_gx1 += 1
                elif (soma_candles_quad_todo < 0):
                    #Com minoria verde
                    if dados[i]['cor'] == 1 : # se a primeira vela do quadrante proximo for verde
                        gx1.append('1')
                        gain_gx1 += 1
                    elif dados[seg_candle]['cor'] == 1 : # se a segunda vela do quadrante proximo for verde
                        gx1.append('+')
                        gale_gx1 +=1
                    else: # se não acontece no proximo quadrante nem na primeira nem na segunda a vela verde
                        gx1.append('0')
                        hit_gx1 += 1
                    # Indefinido - Doji
                elif (soma_candles_quad_todo == 0): # quando for um doji que não permita definir a minoria
                    gx1.append('-')


                #Preenchendo vetor gx2
                
                    #Com minoria vermelha
                if (soma_candles_3Ultimas > 0):
                    if  dados[i]['cor'] == -1 :  # se a primeira vela do quadrante proximo for vermelha
                        gx2.append('1')
                        gain_gx2 += 1
                    elif dados[seg_candle]['cor'] == -1 : # se a segunda vela do quadrante proximo for vermelha
                        gx2.append('+')
                        gale_gx2 += 1
                    else: # se não acontece no proximo quadrante nem na primeira nem na segunda a vela vermelha
                        gx2.append('0')
                        hit_gx2 += 1
                elif (soma_candles_3Ultimas < 0):
                    #Com minoria verde
                    if dados[i]['cor'] == 1 :  # se a primeira vela do quadrante proximo for verde
                        gx2.append('1')
                        gain_gx2 += 1
                    elif dados[seg_candle]['cor'] == 1 : # se a segunda vela do quadrante proximo for verde
                        gx2.append('+')
                        gale_gx2 += 1
                    else: # se não acontece no proximo quadrante nem na primeira nem na segunda a vela verde
                        gx2.append('0')
                        hit_gx2 += 1
                    # Indefinido - Doji
                elif (soma_candles_3Ultimas == 0): # quando for um doji que não permita definir a minoria
                    gx2.append('-')

                #Preenchendo vetor gx3
                    #Com minoria vermelha
                if (dados[i-4]['cor'] == -1):
                    if  dados[i]['cor'] == -1 :  # se a primeira vela do quadrante proximo for vermelha
                        gx3.append('1')
                        gain_gx3 +=1
                    elif dados[seg_candle]['cor'] == -1 : # se a segunda vela do quadrante proximo for vermelha
                        gx3.append('+')
                        gale_gx3 += 1
                    else: # se não acontece no proximo quadrante nem na primeira nem na segunda a vela vermelha
                        gx3.append('0')
                        hit_gx3 +=1
                elif (dados[i-4]['cor'] == 1):
                    #Com minoria verde
                    if dados[i]['cor'] == 1 :  # se a primeira vela do quadrante proximo for verde
                        gx3.append('1')
                        gain_gx3 +=1
                    elif dados[seg_candle]['cor'] == 1 : # se a segunda vela do quadrante proximo for verde
                        gx3.append('+')
                        gale_gx3 += 1
                    else: # se não acontece no proximo quadrante nem na primeira nem na segunda a vela verde
                        gx3.append('0')
                        hit_gx3 +=1
                    # Indefinido - Doji
                elif (dados[i-4]['cor'] == 0): # quando for um doji que não permita definir a minoria
                    gx3.append('-')

            soma_candles_quad_todo = 0
            soma_candles_3Ultimas = 0
        
        # inicia somente quando existir o quadrante inteiro contagem das velas - GX1
        if i >= inicio_contagem: 
            soma_candles_quad_todo += dados[i]['cor']
    # catalog = { "gx1": gx1, "gx2": gx2, "gx3": gx3, "gain_gx1": gain_gx1, 
    #     "gain_gx2" : gain_gx2, "gain_gx3" : gain_gx3,"gale_gx1": gale_gx1, 
    #     "gale_gx2" : gale_gx2, "gale_gx3": gale_gx3,"hit_gx1": hit_gx1, 
    #     "hit_gx2" : hit_gx2, "hit_gx3": hit_gx3 }
    
    # print(gain_gx1)
    # print(gain_gx2)
    # print(gain_gx3)
    # print(gale_gx1)
    # print(gale_gx2)
    # print(gale_gx3)
    # print(hit_gx1)
    # print(hit_gx2)
    # print(hit_gx3)
    # print(gx1)
    # print(gx2)
    # print(gx3)

    
    catalog = { "gain_gx1": gain_gx1,"gain_gx2": gain_gx2,"gain_gx3": gain_gx3,
                "gale_gx1": gale_gx1,"gale_gx2": gale_gx2,"gale_gx3": gale_gx3,
                "hit_gx1": hit_gx1,"hit_gx2": hit_gx2,"hit_gx3": hit_gx3,
                "time_inicio": time_inicio, "time_fim": time_fim }

    return catalog