import sys
from iqoptionapi.stable_api import IQ_Option
from connectionApi import loginApi
from scriptApi import payout
from captura_candles_mhi import dadosMetodo_Gedex

API = loginApi(str(sys.argv[1]), str(sys.argv[2])) 

parit = []

par1 = API.get_all_open_time() # Descobrindo quais paridades estão abertas

for par in par1['turbo']:# retorna ,as que estão abertas, da lista de turbo o campo open 
    if par1['turbo'][par]['open'] == True: 
        parit.append(par)

for i in range(0,len(parit)):
    a = dadosMetodo_Gedex(API, 60, 7200, parit[i])
    totalPartidas = a['gain']+a['gale']+a['hit']
    acertividade = round((((totalPartidas - a['hit'])/totalPartidas)*100),1)
    print(str(parit[i]))
    print(str(acertividade)) 
print(str(a['time_inicio']) + ' às '+str(a['time_fim']))