const PythonShell = require('python-shell');
const options = require('./pythonConfig');


PythonShell.run('test.py', options, function (err, results) {
    if (err) throw err;
    // results is an array consisting of messages collected during execution
    console.log('results: %j', results);
  });

