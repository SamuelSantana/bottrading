const express = require('express');
const path = require('path');
const morgan = require('morgan');
//const mysql = require('mysql')
//const myConnection = require('express-myconnection')
const passport = require('passport');
const session = require('express-session')
const flash = require('connect-flash')
const engine = require('ejs-mate');
const { PythonShell } = require('python-shell');
const crypto = require('./lib/crypto')
var connection = require('./lib/connection')



//inicializações
const app = express();
require('./lib/connection')

//importing routers
const traderRoutes = require('./routes/trader')
const indexRoutes = require('./routes/index')


// settings
app.set('port', process.env.PORT || 4001);
app.engine('ejs', engine)
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs');


//middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }))
app.use(session({
    secret: '!@#estado',
    resave: true,
    saveUninitialized: true
}))
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

require('./lib/passport')
app.use((req, res, next) => {
    app.locals.user = req.user // cria uma variavel global de liberação de rotas para usuários
    app.locals.cadastrarMensagem = req.flash('cadastrarMensagem')
    app.locals.loginMensagem = req.flash('loginMensagem')
    console.log(req.user)
    next()
})



//routes
app.use('/trader', traderRoutes);
app.use('/', indexRoutes);

//static files
app.use(express.static(path.join(__dirname, 'public')));

// starting the server
const server = app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
})


//websockets

const SocketIO = require('socket.io')
const io = SocketIO(server)

io.on('connection', (socket) => {
    console.log('new conection', socket.id)
    socket.on('paridade', async (user) => { // recebe os dados enviados pelo front
        console.log(user.email, user.password)
        var password = crypto.decode(user.password).toString()
        let options = await {
            mode: 'text',
            pythonPath: '/usr/bin/python3.6',
            pythonOptions: ['-u'], // get print results in real-time
            scriptPath: path.join(__dirname, './app/script'),
            args: [user.email, password]
        };

        promisse = new Promise((resolve, reject) => {
            PythonShell.run('paridade.py', options, function (err, results) {
                if (err)
                    reject(err)
                else resolve(results)

                if (err) throw err;
                // results is an array consisting of messages collected during execution
                console.log('results: %j', results);
            });
        })
            .then(async (results) => {
                var paridade = await results
                io.emit('paridade', paridade)
                console.log(results)
                //res.json({status: 'sucesso000000', paridade: paridade})
            })
            .catch((err) => {
                console.log("Erro catch")
            })
    })


    socket.on('acertividade_mhi', async (user) => { // recebe os dados enviados pelo front
        console.log(user.email, user.password)
        var password = crypto.decode(user.password).toString()
        let options = await {
            mode: 'text',
            pythonPath: '/usr/bin/python3.6',
            pythonOptions: ['-u'], // get print results in real-time
            scriptPath: path.join(__dirname, './app/script'),
            args: [user.email, password]
        };

        promisse = new Promise((resolve, reject) => {
            PythonShell.run('acertividade_mhi.py', options, function (err, results) {
                if (err)
                    reject(err)
                else resolve(results)

                if (err) throw err;
                // results is an array consisting of messages collected during execution
                console.log('results: %j', results);
            });
        })
            .then(async (results) => {
                var acertividade_mhi = await results
                io.emit('acertividade_mhi', acertividade_mhi)
                console.log(results)
                //res.json({status: 'sucesso000000', paridade: paridade})
            })
            .catch((err) => {
                console.log("Erro catch")
            })
    })
})