const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs')
const {Schema} = mongoose;

const userSchema = new Schema({
    email: {type: String, required: true},
    password: {type:String, required: true,minlength: 3},
    api: String,
    nome: String,
    cpf: String,
    telefone: String,
    cidade: String,
    ingressou_em: { type: Date, default: Date.now },
    data_vencimento: Date,
    pagou_em : Date,
    primeiro_acesso: {type:Boolean, default: true},
    oculto: {type:Boolean, default: false},
    admin: {type:Boolean, default: false}
})

userSchema.methods.encryptPassword = (password)=>{
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}

userSchema.methods.comparePassword = function(password){
    return bcrypt.compareSync(password, this.password);
}


module.exports = mongoose.model('users', userSchema);